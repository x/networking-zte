============
Installation
============

At the command line::

    $ pip install networking-zte

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv networking-zte
    $ pip install networking-zte
