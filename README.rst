===============================
networking-zte
===============================

This project contains the work to integrate the ZTE SDN controller with Neutron.

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/networking-zte
* Source: http://git.openstack.org/cgit/openstack/networking-zte
* Bugs: http://bugs.launchpad.net/networking-zte

Features
--------

* TODO
